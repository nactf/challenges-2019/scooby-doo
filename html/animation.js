let button = document.getElementById("button");
let container = document.getElementById("animationContainer");
let containerStyle = window.getComputedStyle(container);
var width = containerStyle.width;
width = parseInt(width.substr(0, width.length-2));
var height = containerStyle.height
height = parseInt(height.substr(0, height.length-2));
var buttonTop = "";
var buttonLeft = "";

var buttonHeight = window.getComputedStyle(button).height;
buttonHeight = parseInt(buttonHeight.substr(0, buttonHeight.length-2));
var buttonWidth = window.getComputedStyle(button).width;
buttonWidth = parseInt(buttonWidth.substr(0, buttonWidth.length-2));

var clickCount = 0;

function mouseOver() {
    buttonTop = buttonHeight/2 + Math.floor(Math.random() * (height-buttonHeight));
    buttonLeft = buttonWidth/2 + Math.floor(Math.random() * (width-buttonWidth));
    $( document ).ready(function() {
        $("#button").animate({top: buttonTop + "px", left: buttonLeft + "px"});
    });
   
}

function mouseClick() {
    clickCount ++;
    document.getElementById("score").innerHTML = "Score: " + clickCount;
    
    if (clickCount >= 1000000000) {
        var elements = document.getElementsByClassName('letter');
        for (i = 0; i < elements.length; i++) {
            elements[i].style.opacity = "1";
        }
    }
}